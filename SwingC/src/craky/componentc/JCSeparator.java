package craky.componentc;

import javax.swing.JSeparator;

public class JCSeparator extends JSeparator
{
    private static final long serialVersionUID = -2404686601191054374L;

    public JCSeparator()
    {
        this(HORIZONTAL);
    }

    public JCSeparator(int orientation)
    {
        super(orientation);
        setUI(new CSeparatorUI());
        setOpaque(false);
    }
    
    @Deprecated
    public void updateUI()
    {}
}
