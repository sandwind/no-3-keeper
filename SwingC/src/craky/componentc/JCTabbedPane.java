package craky.componentc;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import craky.util.UIUtil;

public class JCTabbedPane extends JTabbedPane
{
    private static final long serialVersionUID = 7830093625100770074L;
    
    private int tabHeight;

    private Color disabledForeground;
    
    private Map<Component, Color> disabledForegroundMap;
    
    public JCTabbedPane()
    {
        this(TOP, WRAP_TAB_LAYOUT);
    }

    public JCTabbedPane(int tabPlacement)
    {
        this(tabPlacement, WRAP_TAB_LAYOUT);
    }

    public JCTabbedPane(int tabPlacement, int tabLayoutPolicy)
    {
        super(tabPlacement, tabLayoutPolicy);
        setUI(new CTabbedPaneUI());
        setFont(UIUtil.getDefaultFont());
        setForeground(new Color(0, 28, 48));
        setBackground(Color.GRAY);
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setOpaque(false);
        setTabLayoutPolicy(SCROLL_TAB_LAYOUT);
        tabHeight = 24;
        disabledForeground = new Color(123, 123, 122);
        disabledForegroundMap = new HashMap<Component, Color>();
    }

    public int getTabHeight()
    {
        return tabHeight;
    }

    public void setTabHeight(int tabHeight)
    {
        this.tabHeight = tabHeight;
        this.revalidate();
    }

    public Color getDisabledForeground()
    {
        return disabledForeground;
    }

    public void setDisabledForeground(Color disabledForeground)
    {
        this.disabledForeground = disabledForeground;
        this.repaint();
    }
    
    public Color getDisabledForegroundAt(int tabIndex)
    {
        Component c = getComponentAt(tabIndex);
        Color color = c == null? null: disabledForegroundMap.get(c);
        return color == null? disabledForeground: color;
    }

    public void setDisabledForegroundAt(int tabIndex, Color disabledForeground)
    {
        Component c = getComponentAt(tabIndex);
        
        if(c != null)
        {
            disabledForegroundMap.put(c, disabledForeground);
            
            if(!isEnabledAt(tabIndex) || !this.isEnabled())
            {
                this.repaint();
            }
        }
    }
    
    public void removeTabAt(int index)
    {
        Component c = getComponentAt(index);
        
        if(c != null)
        {
            disabledForegroundMap.remove(c);
        }
        
        super.removeTabAt(index);
    }
    
    public void setComponentAt(int index, Component component)
    {
        Component c = getComponentAt(index);
        Color color = null;
        
        if(c != null)
        {
            color = disabledForegroundMap.remove(c);
        }
        
        super.setComponentAt(index, component);
        
        if(color != null)
        {
            disabledForegroundMap.put(component, color);
        }
    }
    
    @Deprecated
    public void updateUI()
    {}
}