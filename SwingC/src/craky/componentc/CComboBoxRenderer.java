package craky.componentc;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.io.Serializable;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicLabelUI;

import sun.awt.AppContext;
import craky.util.UIResourceManager;
import craky.util.UIUtil;

public class CComboBoxRenderer extends JLabel implements ListCellRenderer, Serializable
{
    private static final long serialVersionUID = 3622492300888739118L;
    
    private static final Border NORMAL_BORDER = UIResourceManager.getBorder(UIResourceManager.KEY_COMBO_BOX_RENDERER_BORDER);
    
    public static final Border SELECTED_BORDER = UIResourceManager.getBorder(UIResourceManager.KEY_COMBO_BOX_RENDERER_SELECTED_BORDER);
    
    private JComboBox combo;

    public CComboBoxRenderer(JComboBox combo)
    {
        super();
        setUI(new RendererUI());
        this.combo = combo;
    }

    public Dimension getPreferredSize()
    {
        Dimension size;

        if((this.getText() == null) || (this.getText().isEmpty()))
        {
            setText(" ");
            size = super.getPreferredSize();
            setText("");
        }
        else
        {
            size = super.getPreferredSize();
        }
        
        if(size.height < 20)
        {
            size.height = 20;
        }
        
        return size;
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if(value instanceof Icon)
        {
            setIcon((Icon)value);
        }
        else
        {
            setText((value == null)? "": value.toString());
        }
        
        setBorder(NORMAL_BORDER);
        setOpaque(isSelected);
        setFont(combo.getFont());
        setForeground(isSelected? UIResourceManager.getColor(UIResourceManager.KEY_TEXT_SELECTION_FOREGROUND): combo.getForeground());
        return this;
    }
    
    @Deprecated
    public void updateUI()
    {}
    
    private static class RendererUI extends BasicLabelUI
    {
        protected static RendererUI rendererUI = new RendererUI();

        private static final Object RENDERER_UI_KEY = new Object();
        
        private static final Image BG_IMAGE = UIResourceManager.getImage(UIResourceManager.KEY_SELECTED_ITEM_BACKGROUND_IMAGE);
        
        public static ComponentUI createUI(JComponent c)
        {
            if(System.getSecurityManager() != null)
            {
                AppContext appContext = AppContext.getAppContext();
                RendererUI safeRendererUI = (RendererUI)appContext.get(RENDERER_UI_KEY);
                
                if(safeRendererUI == null)
                {
                    safeRendererUI = new RendererUI();
                    appContext.put(RENDERER_UI_KEY, safeRendererUI);
                }
                
                return safeRendererUI;
            }
            
            return rendererUI;
        }
        
        public void update(Graphics g, JComponent c)
        {
            if(c.isOpaque())
            {
                paintBackground(g, c);
            }
            
            paint(g, c);
        }
        
        private void paintBackground(Graphics g, JComponent c)
        {
            Rectangle paintRect = new Rectangle(0, 0, c.getWidth(), c.getHeight());
            UIUtil.paintImage(g, BG_IMAGE, new Insets(1, 1, 1, 1), paintRect, c);
        }
        
        protected void installDefaults(JLabel c)
        {}

        protected void uninstallDefaults(JLabel c)
        {}
    }
}