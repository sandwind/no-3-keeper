package craky.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
    /**
     * CSV文件中需要转义的字符集正则表达式
     */
    public static final String CSV_REGEX = ".*([\"|\\,|\\n|\\r]|(\\r\\n)|(\\n\\r)).*";

    /**
     * 将字节数组转换成16进制字符串
     * @param bytes 目标字节数组
     * @return 转换结果
     */
    public static String bytesToHex(byte bytes[])
    {
        return bytesToHex(bytes, null);
    }

    /**
     * 将字节数组转换成16进制字符串
     * @param bytes 目标字节数组
     * @param split 16进制字符串的分隔符
     * @return 转换结果
     */
    public static String bytesToHex(byte bytes[], String split)
    {
        return bytesToHex(bytes, 0, bytes.length, split);
    }

    /**
     * 将字节数组中指定区间的字节转换成16进制字符串
     * @param bytes 目标字节数组
     * @param start 起始位置（包括该位置）
     * @param end 结束位置（不包括该位置）
     * @param split 16进制字符串的分隔符
     * @return 转换结果
     */
    public static String bytesToHex(byte bytes[], int start, int end, String split)
    {
        StringBuilder sb = new StringBuilder();
        boolean hasSplit = split != null && !split.isEmpty();
        end = Math.min(end, bytes.length);

        for(int i = start; i < end; i++)
        {
            if(hasSplit && i > start)
            {
                sb.append(split);
            }

            sb.append(byteToHex(bytes[i]));
        }

        return sb.toString();
    }

    /**
     * 将单个字节码转换成16进制字符串
     * @param b 目标字节
     * @return 转换结果
     */
    public static String byteToHex(byte b)
    {
        String hex = Integer.toHexString(0xFF & b | 0x00);
        return b >= 0 && b <= 15? '0' + hex: hex;
    }

    /**
     * 将普通字符串转换为符合CSV格式的字符串
     * @param str 普通字符串
     * @return 符合CSV格式的字符串
     */
    public static String toCSVString(String str)
    {
        str = str.replaceAll("\"", "\"\"");
        Pattern p = Pattern.compile(CSV_REGEX, Pattern.DOTALL);
        Matcher m = p.matcher(str);

        if(m.matches())
        {
            str = "\"" + str + "\"";
        }

        return str;
    }
}