@color 0A
@set name=NO.3 Keeper Server 1.0
@title %name%
@echo %name%&echo.
@set CLASSPATH=lib\KeeperServer.jar;lib\SmallSQL.jar;lib\hessian-4.0.37.jar;lib\jetty-continuation-8.1.16.v20140903.jar;lib\jetty-http-8.1.16.v20140903.jar;lib\jetty-io-8.1.16.v20140903.jar;lib\jetty-security-8.1.16.v20140903.jar;lib\jetty-server-8.1.16.v20140903.jar;lib\jetty-servlet-8.1.16.v20140903.jar;lib\jetty-util-8.1.16.v20140903.jar;lib\servlet-api-3.0.jar;
@if exist jre (goto start1) else (goto start2)
:start1
@jre\bin\java craky.keeper.KeeperServer
:start2
@java craky.keeper.KeeperServer