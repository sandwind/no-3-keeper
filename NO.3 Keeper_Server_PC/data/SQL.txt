CREATE TABLE "user" ("id" int NOT NULL auto_increment PRIMARY KEY,"name" varchar(25) NOT NULL default '',"password" varchar(100) NOT NULL default '',"purview" int NOT NULL default '0');

CREATE TABLE "category" ("id" int NOT NULL auto_increment PRIMARY KEY,"name" varchar(25) NOT NULL default '',"type" int NOT NULL default '0',"count" bigint NOT NULL default '0');
INSERT INTO "category" ("name","type","count") VALUES ('服饰',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('餐饮',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('交通',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('通讯',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('水费',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('电费',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('燃气费',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('住房',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('额外',0,0);
INSERT INTO "category" ("name","type","count") VALUES ('其他',0,0);

INSERT INTO "category" ("name","type","count") VALUES ('工资',100,0);
INSERT INTO "category" ("name","type","count") VALUES ('奖金',100,0);
INSERT INTO "category" ("name","type","count") VALUES ('额外',100,0);
INSERT INTO "category" ("name","type","count") VALUES ('其他',100,0);

CREATE TABLE "pay" ("id" int NOT NULL auto_increment PRIMARY KEY,"date" date NOT NULL,"amount" real NOT NULL,"summary" varchar(25) NOT NULL,"type" varchar(25) NOT NULL,"detail" longnvarchar,"remark" longnvarchar,"recordTime" timestamp NOT NULL,"recorder" varchar(25) NOT NULL);

CREATE TABLE "income" ("id" int NOT NULL auto_increment PRIMARY KEY,"date" date NOT NULL,"amount" real NOT NULL,"summary" varchar(25) NOT NULL,"type" varchar(25) NOT NULL,"detail" longnvarchar,"remark" longnvarchar,"recordTime" timestamp NOT NULL,"recorder" varchar(25) NOT NULL);