package craky.keeper.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import craky.component.JImagePane;
import craky.componentc.JCButton;
import craky.componentc.JCDialog;
import craky.componentc.JCIPV4AddressField;
import craky.componentc.JCLabel;
import craky.keeper.KeeperApp;
import craky.keeper.LoginFrame;
import craky.keeper.pane.EmptyComponent;
import craky.keeper.pane.KeeperGlassPane;
import craky.keeper.util.KeeperConst;
import craky.keeper.util.KeeperUtil;
import craky.layout.LinearLayout;
import craky.util.UIUtil;

public class ServerConfigDialog extends JCDialog implements ActionListener
{
    private static final long serialVersionUID = 4787072148793183017L;

    private static final String DEFAULT_ERROR = "\u8FDE\u63A5\u670D\u52A1\u5668\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5\u6216\u66F4\u6539IP\u5730\u5740\u3002";

    private KeeperApp keeper;

    private JComponent content;

    private JCButton btnOk, btnCancel;

    private JCIPV4AddressField txtIP;

    private JCLabel lbError;

    private KeeperGlassPane glassPane;

    public ServerConfigDialog(KeeperApp keeper, Window owner, String ip)
    {
        super(owner, "\u670D\u52A1\u5668\u8BBE\u7F6E", ModalityType.DOCUMENT_MODAL);
        this.keeper = keeper;
        this.initUI(ip != null);
        String bannerName = null;

        if(owner == null)
        {
            setIconImage(KeeperUtil.getImage("logo_16.png"));
        }

        if((owner == null || owner instanceof LoginFrame) && (bannerName = keeper.getBannerName()) != null)
        {
            this.setBackgroundImage(new ImageIcon(KeeperConst.BANNER_BG_DIR + KeeperConst.FILE_SEP + bannerName).getImage());
        }

        if(ip != null)
        {
            txtIP.setIpAddress(ip);
        }
    }

    private void initUI(boolean hasError)
    {
        content = (JComponent)this.getContentPane();
        JImagePane mainPane = new JImagePane();
        EmptyComponent ecIP = new EmptyComponent();
        JImagePane buttonPane = new JImagePane();
        JCLabel lbIP = new JCLabel("IP\u5730\u5740\uFF1A");
        lbError = new JCLabel(DEFAULT_ERROR);
        txtIP = new JCIPV4AddressField();
        btnOk = new JCButton("\u6D4B\u8BD5\u8FDE\u63A5");
        btnCancel = new JCButton("\u53D6\u6D88");

        content.setBorder(new EmptyBorder(0, 2, 2, 2));
        content.setPreferredSize(new Dimension(310, 150));
        content.setLayout(new BorderLayout());
        mainPane.setLayout(new LinearLayout(7, 15, 15, 15, 0, LinearLayout.LEADING, LinearLayout.LEADING, LinearLayout.VERTICAL));
        mainPane.setBackground(new Color(255, 255, 255, 210));
        ecIP.setLayout(new BorderLayout());
        ecIP.setPreferredSize(new Dimension(-1, 23));
        lbError.setForeground(new Color(255, 40, 110));
        lbError.setPreferredSize(ecIP.getPreferredSize());
        lbError.setVisible(hasError);
        buttonPane.setLayout(new LinearLayout(5, 7, 7, 7, 0, LinearLayout.LEADING, LinearLayout.LEADING, LinearLayout.HORIZONTAL));
        buttonPane.setBackground(new Color(255, 255, 255, 150));
        buttonPane.setPreferredSize(new Dimension(-1, 32));
        lbIP.setPreferredSize(new Dimension(60, -1));
        btnOk.setPreferredSize(new Dimension(90, 21));
        btnOk.addActionListener(this);
        btnCancel.setPreferredSize(new Dimension(69, 21));
        btnCancel.addActionListener(this);

        ecIP.add(lbIP, BorderLayout.WEST);
        ecIP.add(txtIP, BorderLayout.CENTER);
        buttonPane.add(btnOk, LinearLayout.END);
        buttonPane.add(btnCancel, LinearLayout.END);
        mainPane.add(ecIP, LinearLayout.START_FILL);
        mainPane.add(lbError, LinearLayout.START_FILL);
        content.add(buttonPane, BorderLayout.SOUTH);
        content.add(mainPane, BorderLayout.CENTER);
        this.setGlassPane(glassPane = new KeeperGlassPane(this));
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(this.getOwner());
        txtIP.requestFocus();
        UIUtil.escAndEnterAction(this, btnOk, new AbstractAction()
        {
            private static final long serialVersionUID = 7092223290373790975L;

            public void actionPerformed(ActionEvent e)
            {
                cancel();
            }
        });

        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                cancel();
            }
        });
    }

    private void submit()
    {
        final String ip = txtIP.getIpAddress();

        if(ip.isEmpty() || ip.startsWith("0."))
        {
            lbError.setText("IP\u5730\u5740\u9519\u8BEF\uFF01");
            lbError.setVisible(true);
        }
        else
        {
            Runnable run = new Runnable()
            {
                public void run()
                {
                    try
                    {
                        lbError.setVisible(false);
                        keeper.testConnection(ip);
                        keeper.load();
                    }
                    catch(Exception e)
                    {
                        lbError.setText(DEFAULT_ERROR);
                        lbError.setVisible(true);
                    }
                }
            };
            glassPane.doTask("\u6B63\u5728\u8FDE\u63A5\u670D\u52A1\u5668\u2026\u2026", run, btnOk);
        }
    }

    private void cancel()
    {
        this.dispose();
    }

    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();

        if(source == btnOk)
        {
            submit();
        }
        else if(source == btnCancel)
        {
            cancel();
        }
    }
}
