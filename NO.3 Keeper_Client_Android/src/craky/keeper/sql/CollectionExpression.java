package craky.keeper.sql;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

public abstract class CollectionExpression implements Criterion, Serializable
{
    private static final long serialVersionUID = 7958063180722820844L;

    private String columnName;

    private Object[] values;

    protected CollectionExpression(String columnName, Object[] values)
    {
        this.columnName = columnName;
        this.values = values;
    }

    public String toSqlString()
    {
        StringBuilder params = new StringBuilder();

        for(int i = 0; i < values.length; i++)
        {
            if(i > 0)
            {
                params.append(',');
                params.append(' ');
            }

            params.append('?');
        }

        return columnName + " " + getOperation() + " (" + params + ')';
    }

    protected abstract String getOperation();

    public Object getValue()
    {
        return null;
    }

    public Collection<?> getValues()
    {
        return values.length > 0? Arrays.asList(values): null;
    }
}