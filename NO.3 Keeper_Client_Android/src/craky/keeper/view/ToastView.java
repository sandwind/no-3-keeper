package craky.keeper.view;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import craky.keeper.client.R;
import craky.keeper.util.KeeperUtil;

public class ToastView extends RelativeLayout
{
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(2.6f);

    private ImageView toastImage;

    private TextView toastText;

    private Timer delayHideTimer;

    private TimerTask delayHideTimerTask;

    public ToastView(final Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.delayHideTimer = new Timer(true);
        this.post(new Runnable()
        {
            public void run()
            {
                Activity activity = (Activity)context;
                toastImage = (ImageView)activity.findViewById(R.id.toastImage);
                toastText = (TextView)activity.findViewById(R.id.toastText);
            }
        });
    }

    private void hideToast()
    {
        this.clearAnimation();
        Animation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(300);
        this.startAnimation(animation);
        this.setVisibility(View.INVISIBLE);
    }

    public void hideToastImmediately()
    {
        if(delayHideTimerTask != null)
        {
            delayHideTimerTask.cancel();
        }

        this.clearAnimation();
        this.setVisibility(View.INVISIBLE);
    }

    public void showToast(int iconId, String message)
    {
        this.hideToastImmediately();
        toastImage.setImageResource(iconId);
        toastText.setText(message);
        int animationType = Animation.RELATIVE_TO_SELF;
        Animation animation = new TranslateAnimation(animationType, 0.0f, animationType, 0.0f, animationType, 1.0f, animationType, 0.0f);
        animation.setDuration(350);
        animation.setInterpolator(OVERSHOOT_INTERPOLATOR);
        this.startAnimation(animation);
        this.setVisibility(View.VISIBLE);
        delayHideTimer.schedule(delayHideTimerTask = new TimerTask()
        {
            public void run()
            {
                post(new Runnable()
                {
                    public void run()
                    {
                        hideToast();
                        delayHideTimerTask = null;
                    }
                });
            }
        }, 2000);
    }

    public void startTask(int iconId, String message)
    {
        this.hideToastImmediately();
        toastImage.setImageResource(iconId);
        toastText.setText(message);
        float pivot = toastImage.getWidth() / 2.0f;
        Animation animation = new RotateAnimation(0, Integer.MAX_VALUE / 3.0f, pivot, pivot);
        animation.setInterpolator(KeeperUtil.LINEAR_INTERPOLATOR);
        animation.setDuration(Integer.MAX_VALUE);
        this.setVisibility(View.VISIBLE);
        toastImage.startAnimation(animation);
    }

    public void stopTask()
    {
        toastImage.clearAnimation();
        this.setVisibility(View.INVISIBLE);
    }
}