package craky.keeper.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.util.AttributeSet;
import android.widget.Button;
import craky.keeper.util.KeeperUtil;

public class BackButton extends Button
{
    private Paint paint;

    private ColorStateList colorStateList;

    public BackButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(1.5f * KeeperUtil.density);
        paint.setStrokeJoin(Join.MITER);
        paint.setStrokeCap(Cap.SQUARE);
        colorStateList = this.getTextColors();
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        paint.setColor(colorStateList.getColorForState(this.getDrawableState(), 0));
        int height = canvas.getHeight();
        float vCenter = height / 2.0f;
        float iconWidth = height * 24 / 43;
        float startX = 1;
        float[] pts = {iconWidth, 0, startX, vCenter, startX, vCenter, iconWidth, height};
        canvas.drawLines(pts, paint);
    }
}