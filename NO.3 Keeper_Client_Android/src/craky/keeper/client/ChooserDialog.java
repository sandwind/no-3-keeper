package craky.keeper.client;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import craky.keeper.util.KeeperUtil;

public class ChooserDialog extends DialogFragment implements View.OnClickListener
{
    private Activity context;

    private Dialog dialog;

    private String[] chooserItems;

    private String title;

    private EditText textView;

    private WheelView wheelView;

    public ChooserDialog(Activity context, String[] chooserItems, String title, EditText textView)
    {
        this.context = context;
        this.chooserItems = chooserItems;
        this.title = title;
        this.textView = textView;
    }

    public void reset(String[] chooserItems, String title, EditText textView)
    {
        this.chooserItems = chooserItems;
        this.title = title;
        this.textView = textView;
    }

    @SuppressLint("InflateParams")
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        if(dialog == null)
        {
            dialog = new Dialog(context, R.style.ChooserDialog);
            View view = context.getLayoutInflater().inflate(R.layout.chooser_dialog, null);
            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            Point screenSize = new Point();
            context.getWindowManager().getDefaultDisplay().getSize(screenSize);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(view);
            layoutParams.gravity = Gravity.FILL_HORIZONTAL | Gravity.BOTTOM;
            layoutParams.width = screenSize.x;
            layoutParams.height = view.getMinimumHeight();
            wheelView = (WheelView)dialog.findViewById(R.id.wheelView);
            dialog.findViewById(R.id.btnOk).setOnClickListener(this);
        }

        TextView txtTitle = (TextView)dialog.findViewById(R.id.title);
        txtTitle.setText(title);
        wheelView.setViewAdapter(new ChooserArrayAdapter(context, chooserItems));
        String selectedText = textView.getText().toString();

        if(!selectedText.isEmpty())
        {
            int size = chooserItems.length;
            int index = -1;

            for(int i = 0; i < size; i++)
            {
                if(selectedText.equals(chooserItems[i]))
                {
                    index = i;
                    break;
                }
            }

            wheelView.setCurrentItem(index);
        }

        return dialog;
    }

    @Override
    public void onClick(View view)
    {
        int selectedItem = wheelView.getCurrentItem();

        if(selectedItem >= 0)
        {
            textView.setText(chooserItems[selectedItem]);
        }

        dialog.dismiss();
    }

    private class ChooserArrayAdapter extends ArrayWheelAdapter<String>
    {
        public ChooserArrayAdapter(Context context, String[] items)
        {
            super(context, items);
        }

        @Override
        protected void configureTextView(TextView view)
        {
            super.configureTextView(view);
            view.setTextColor(0xFF6A6A6A);
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17.5f);
            view.setTypeface(null, 0);
            view.setHeight((int)(32 * KeeperUtil.density));
        }

        @Override
        public void configureTextView(int index, TextView view)
        {
            super.configureTextView(index, view);

            if(index == wheelView.getCurrentItem())
            {
                view.setTextColor(0xFF00A5E0);
            }
            else
            {
                view.setTextColor(0xFF6A6A6A);
            }
        }
    }
}